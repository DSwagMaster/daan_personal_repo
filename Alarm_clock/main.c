#include <msp430.h> 
#include <stdint.h>

/* Test SPI code
 * trying to successfully connect
 * with 8x8 led matrix, controlled
 * by a MAX7219 chip.
 * Checked my progress with this guy's code:
 * https://github.com/evilbetty/msp430_max7219_ledmatrix/blob/master/main.c
 */

#define SPI_MISO    BIT6
//#define SPI_MOSI    BIT7 //Might not need this one?
#define SPI_CLK     BIT5
#define SPI_CS      BIT4 //Regular I/O (?)

/*----Registers----*/
#define MAX_NOOP    0x00
#define MAX_DIGIT0  0x01
#define MAX_DIGIT1  0x02
#define MAX_DIGIT2  0x03
#define MAX_DIGIT3  0x04
#define MAX_DIGIT4  0x05
#define MAX_DIGIT5  0x06
#define MAX_DIGIT6  0x07
#define MAX_DIGIT7  0x08
#define MAX_DECODEMODE  0x09
#define MAX_INTENSITY   0x0A
#define MAX_SCANLIMIT   0x0B
#define MAX_SHUTDOWN    0x0C
#define MAX_DISPLAYTEST 0x0F

/*----Function declaration----*/
void InitSPI(CS, MOSI, CLK);
void TransferSPI(address, data);

const uint8_t number[10][8] = {
    { 0x3c, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x3c }, // 0
    { 0x18, 0x38, 0x18, 0x18, 0x18, 0x18, 0x18, 0x3c }, // 1
    { 0x3c, 0x66, 0x06, 0x06, 0x0c, 0x18, 0x30, 0x7e }, // 2
    { 0x3c, 0x66, 0x06, 0x1e, 0x1e, 0x06, 0x66, 0x3c }, // 3
};

int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
 //1Mhz
	if (CALBC1_1MHZ==0xFF)      // If calibration constant erased
	{
	    while(1);                               // do not load, trap CPU!!
    }
    DCOCTL = 0;                               // Select lowest DCOx and MODx settings
    BCSCTL1 = CALBC1_1MHZ;                    // Set range
    DCOCTL = CALDCO_1MHZ;                     // Set DCO step + modulation */

    InitSPI(SPI_CS, SPI_MISO, SPI_CLK);

    TransferSPI(MAX_NOOP, 0);
    TransferSPI(MAX_DECODEMODE, 0); //No BCD
    TransferSPI(MAX_INTENSITY, 0x07); //intensity from 0x01 to 0x0F
    TransferSPI(MAX_SCANLIMIT, 0x07); //Idk yet...

    //test
    uint8_t i;
    for(i = 0 ; i < 8 ; ){
        TransferSPI(MAX_DIGIT0 + i, number[0][i]);
        __delay_cycles(500000);
    }
    __delay_cycles(1000000);

    while(1){

    }

	return 0;
}

void InitSPI(CS, MOSI, CLK){
    //step 1
    UCB0CTL1 |= UCSWRST; //reset state

    //step 2
    UCB0CTL0 |= UCCKPH | UCMSB | UCMST | UCSYNC;//leading edge (?) | MSB | Master mode | synchronous
    UCB0CTL1 |= UCSSEL_2;

    UCB0BR0 |= 0x01;
    UCB0BR1 = 0;

    //step3
    P1SEL |= MISO | CS | CLK;     //secundary pin functions
    P1SEL2 |= MISO | CS | CLK;

    //step 4
    UCB0CTL1 &= ~UCSWRST; //start state

    //step 5
    P1DIR |= MISO | CS | CLK;
    P1OUT |= CS;
}

void TransferSPI(address, data){
    UCB0TXBUF = (address<<8) | data;
    while((UCB0STAT && UCBUSY);
}
