#include <msp430.h>

int i = 0;
int t = 0;

#pragma vector = TIMER0_A1_VECTOR
__interrupt void timer(void){
    i++;
    if (i >= 75){
        if (t == 0){
            TA0CCR1++;
        }
        if (t == 1){
            TA0CCR1--;
        }
        if (TA0CCR1 >= 15){
            t = 1;
        }
        if (TA0CCR1 <= 1){
            t = 0;
        }
        i = 0;
    }
    TA0CTL &= ~TAIFG;
}


/**
 * main.c
 */
int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer

//1Mhz
	if (CALBC1_1MHZ==0xFF)                    // If calibration constant erased
	{
    while(1);                               // do not load, trap CPU!!
	}
	DCOCTL = 0;                               // Select lowest DCOx and MODx settings
	BCSCTL1 = CALBC1_1MHZ;                    // Set range
	DCOCTL = CALDCO_1MHZ;                     // Set DCO step + modulation */

	BCSCTL1 |= XT2OFF; //shuts off XT2
	BCSCTL2 |= SELS;
	BCSCTL3 |= LFXT1S_2; //VLO(!)

	TA0CTL |= TASSEL_1 | ID_0 | MC_1 | TAIE;
	TA0CTL &= ~(TAIFG);
	TA0CCR0 = 15;
	TA0CCR1 = 1;
	TA0CCTL0 |= OUTMOD_4;
	TA0CCTL1 |= OUTMOD_7;

	P1OUT = 0;
	P1IN = 0;
    P1DIR |= BIT1 | BIT2;
    P1SEL |= BIT1 | BIT2;
    P1SEL2 &= ~(BIT1 | BIT2);
    P1OUT &= ~(BIT1 | BIT2);

    __enable_interrupt();
    while(1);


	return 0;
}
