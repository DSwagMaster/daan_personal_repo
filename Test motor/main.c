//Daan van Veldhoven
#include <msp430.h> 
#include <stdint.h>
#include <string.h>
#include "lib/gpio.h"
#include "lib/i2c.h"
#include "lib/oled.h"
#include "lib/fonts.h"


/*Pinout*/
/*
 * Vcc ->   Vcc     | Gnd ->    Gnd
 * ... ->   1.0     | ... ->    Xin
 * ... ->   1.1     | ... ->    Xout
 * Mtr ->   1.2     | Tst ->    Tst
 * ... ->   1.3     | Rst ->    Rst
 * ... ->   1.4     | SCL ->    1.7
 * ... ->   1.5     | SDA ->    1.6
 * ... ->   2.0     | Button ->    2.5
 * ... ->   2.1     | ... ->    2.4
 * ... ->   2.2     | ... ->    2.3
 */



/*Defines*/
#define speed_button    BIT5

/*End defines*/

/*Globals*/
int i = 0;
int t = 0;
int global_timer_mili = 0;
int global_timer_sec = 0;
int global_timer_min = 0;
int global_timer_hrs = 0;
int global_red = 33;
int global_green = 7;
int global_blue = 9;
int global_number = -12;
/*End globals*/

/*Enumerators*/
typedef enum{ //determine next layout when timer overflows.
    second,
    minute,
    hour,
}time;
time time_overflow = second;

typedef enum{
    cm,
    m,
    km,
}distance;
distance distance_overflow = cm;

typedef enum{
    up,
    down,
}pwm;
pwm up_down = up;

typedef enum{
    regular,
    fast,
}speed_bot;
speed_bot current_speed = regular;
/*End enumerators*/

/*Pragmatics*/
#pragma vector = PORT2_VECTOR
__interrupt void inputs(void){
    if((P2IFG & speed_button) != 0){
        switch_speed();
//        pwm_test();
    }
    P2IFG &= ~speed_button;
}

#pragma vector = TIMER0_A1_VECTOR
__interrupt void timer(void){
    global_timer_mili += 10;
//    if(global_timer_mili = 670){
//        oledPrint_time();       // dit is te snel...
//    }
    if(global_timer_mili >= 1000) //1000ms -> 1sec
    {
        global_timer_mili = 0;
        global_timer_sec++;
        oledPrint_time();
        speed();
//        pwm_test();
    }
    if(global_timer_sec >= 60) //60sec -> 1min
    {
        global_timer_sec = 0;
        global_timer_min++;
        oledPrint_time();
    }
    if(global_timer_min >= 60) //60min -> 1hr
    {
        global_timer_min = 0;
        global_timer_hrs++;
        oledPrint_time();
    }
    TA0CTL &= ~TAIFG;
}
/*End pragmatics*/

/*Main*/
int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer

//1Mhz
    if (CALBC1_1MHZ==0xFF)                    // If calibration constant erased
    {
    while(1);                               // do not load, trap CPU!!
    }
    DCOCTL = 0;                               // Select lowest DCOx and MODx settings
    BCSCTL1 = CALBC1_1MHZ;                    // Set range
    DCOCTL = CALDCO_1MHZ;                     // Set DCO step + modulation */

    TA0CTL |= TASSEL_2 | MC_1 | TAIE;
    TA0CTL &= ~TAIFG;
    TA0CCTL1 |= OUTMOD_7;
    TA0CCR0 = 10000; // 1M / 10K = 100 ticks = 1ms
    TA0CCR1 = 7000; //Duty_cycle = 70%


    P2DIR &= ~speed_button; //input at P2.5
    P2REN |= speed_button; //internal resistor enable
    P2OUT &= ~speed_button; //pull-down
    P2IE |= speed_button;
    P2IES &= ~speed_button;
    P2IFG &= ~speed_button;
    P1DIR |= BIT2;
    P1SEL |= BIT2;
    P1SEL2 &= ~BIT2;
    P1OUT = 0;

    oledInitialize();
    oledClearScreen();
    __enable_interrupt();
    oledPrint_standard_boxes1(0,0,127,5);
    oledPrint(5,1,"Tijd",small);
    oledPrint(75, 1, "afstand",small);
    oledPrint_standard_boxes2(0,5,127,15);
    oledPrint_time();
    oledPrint_nfc();
    current_speed = regular;
    while(1){
    }
    return 0;
}
/*End main*/

/*Functions*/

void switch_speed(){
    switch(current_speed){
    case regular:
        current_speed = fast;
        break;
    case fast:
        current_speed = regular;
    }
}

void speed(){
    if(current_speed == regular){
        TA0CCR1 = 6000;
    }
    else if(current_speed == fast){
        TA0CCR1 = 8000;
    }
}

void pwm_test(void){
    switch (up_down){
    case up:
        TA0CCR1 += 1000;
        if (TA0CCR1 >= 10000)
        {
            up_down = down;
        }
        break;
    case down:
        TA0CCR1 -= 1000;
        if (TA0CCR1 <= 1000)
        {
            up_down = up;
        }
        break;
    }
//    i++;
//    if (i >= 1){
//        if (t == 0){
//            TA0CCR1+= 100;
//        }
//        if (t == 1){
//            TA0CCR1-= 100;
//        }
//        if (TA0CCR1 >= 900){
//            t = 1;
//        }
//        if (TA0CCR1 <= 1){
//            t = 0;
//        }
//        i = 0;
//    }
}

void oledPrint_standard_boxes1(start_x,start_y,stop_x,stop_y){
    int i;
    oledFillBox(start_x, start_y, stop_x, start_y, 0x01); //horizontaal boven
    for (i = start_y; i < (stop_y - start_y); i++){
        oledFillBox(start_x, start_y + i, start_x, start_y + i, 0xFF); //verticaal links
        oledFillBox(stop_x, start_y + i, stop_x, start_y + i, 0xFF); //verticaal rechts
        oledFillBox(70, start_y + i, 70, start_y + i, 0xAA); //verticaal op x= 70
    }
    oledFillBox(start_x, stop_y, stop_x, stop_y, 0x01); //horizontaal onder
//    for (i = start_x; i < stop_x; i++){
//    oledSetBufferPixel(i,start_y);      //horizontale lijn onder
//    }
//    for (i = start_y; i < stop_y; i++){
//        oledSetBufferPixel(start_x,i); //verticale lijn links
//    }
//    for (i = start_y; i < stop_y; i++){
//        oledSetBufferPixel(stop_x,i); //verticale lijn rechts
//    }
//    for (i = start_x; i < stop_x; i++){
//    oledSetBufferPixel(i,stop_y);      //horizontale lijn boven
//    }
}

void oledPrint_standard_boxes2(start_x,start_y,stop_x,stop_y){
    int i;
    oledFillBox(start_x, start_y, stop_x, start_y, 0x01); //horizontaal boven
    for (i = start_y; i < (stop_y - start_y); i++){
        oledFillBox(start_x, start_y + i, start_x, start_y + i, 0xFF); //verticaal links
        oledFillBox(stop_x, start_y + i, stop_x, start_y + i, 0xFF); //verticaal rechts
    }
    oledFillBox(start_x, stop_y, stop_x, stop_y, 0x01); //horizontaal onder
}

void oledPrint_time(void){
    char hours[3];
    char minutes[3];
    char seconds[3];
    char mili_seconds[4];
    itoa(global_timer_hrs, hours);
    itoa(global_timer_min, minutes);
    itoa(global_timer_sec, seconds);
    itoa(global_timer_mili, mili_seconds);

    oledClearBox(5, 3, 64, 3);

    oledPrint(5, 3, hours, small);
    oledPrint(12, 3, ":", small);
    oledPrint(15, 3, minutes, small);
    oledPrint(27, 3, ":", small);
    oledPrint(32, 3, seconds, small);
    oledPrint(45, 3, ":", small);
    oledPrint(49, 3, mili_seconds, small);

//    switch(time_overflow){
//    case second:
//        //layout
//        break;
//
//    case minute:
//        //layout
//        break;
//
//    case hour:
//        //layout
//        break;
//
//    }
}

void oledPrint_distance(distance distance_overflow){

    switch (distance_overflow){
    case cm:
        //layout
        break;

    case m:
        //layout
        break;

    case km:
        //layout
        break;

    }
}

void oledPrint_nfc(void){
//    global_red = ;
//    global_green = ;
//    global_blue = ;
//    global_number = ;
    char aantal_rood[3];
    char aantal_groen[3];
    char aantal_blauw[3];
    char x[3];
    itoa(global_red, aantal_rood);
    itoa(global_green, aantal_groen);
    itoa(global_blue, aantal_blauw);
    itoa(global_number, x);

    oledClearBox(5,6,113,6);

    oledPrint(5, 6, "R=", small);
    oledPrint(19, 6, aantal_rood, small);
    oledPrint(34, 6, "G=", small);
    oledPrint(48, 6, aantal_groen, small);
    oledPrint(58, 6, "B=", small);
    oledPrint(72, 6, aantal_blauw, small);
    oledPrint(82, 6, "x=", small);
    oledPrint(96, 6, x, small);


}
/*End functions*/
