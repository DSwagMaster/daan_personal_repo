#ifndef _SPI_H_INCLUDED
#define _SPI_H_INCLUDED

#include <msp430.h>
#include <inttypes.h>

inline static void SPI_begin();
inline static uint8_t SPI_transfer(uint8_t data);
inline static void SPI_end();

/*SPI is verschoven van B naar A*/
void SPI_begin(void){
    /* Configure P1.1, 1.2, 1.4 for SPI. */
    P1SEL |= (BIT1 | BIT2 | BIT4);  //1.1: miso; 1.2: mosi; 1.4: clock
    P1SEL2 |= (BIT1 | BIT2 | BIT4); //1.1: miso; 1.2: mosi; 1.4: clock

    /* Setup SPI on USCI A */
    UCA0CTL1 = (UCSSEL_2 | UCSWRST);               // SMCLK + Reset
    UCA0CTL0 = (UCCKPL | UCMSB | UCMST | UCSYNC);  // 3-pin, 8-bit SPI master
    UCA0BR0 = 8;                                   // /8
    UCA0BR1 = 0;                                   //

    UCA0CTL1 &= ~UCSWRST;                          // release USCI for operation
}

uint8_t SPI_transfer(uint8_t data) {
    UCA0TXBUF = data; // Setting TXBUF clears the TXIFG

    while(UCA0STAT & UCBUSY); // wait for SPI TX/RX to finish

    return UCA0RXBUF; // Reading clears RXIFG
}

void SPI_end()
{
    UCA0CTL1 |= UCSWRST;                // Put USCI in reset mode
}

#endif
