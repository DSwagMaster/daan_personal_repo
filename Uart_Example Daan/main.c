#include <msp430.h> 
#include <stdint.h>

/* UART test
 * This is a UART test code.
 * It will (try to) connect with
 * the hc-05 bluetooth module
 */

#define UART_RXD BIT1
#define UART_TXD BIT2
#define LED      BIT3

char message = 'T';

/*----Function declaration----*/
void InitUART(rxd, txd);
void writeUART(char message);
//void readUART(void);

#pragma vector = USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
    P2OUT ^= LED;
    UCA0TXBUF = UCA0RXBUF;
}
//#pragma USCIAB0RX_VECTOR
//__interrupt void  USCI0TX_ISR(void)
//{
//    UCA0TXBUF = message;
//}

int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
	
	InitUART(UART_RXD,UART_TXD);

	P2DIR |= LED;
	P2OUT &= ~LED;
	IFG2 &= ~(UCA0RXIFG | UCA0TXIFG);

	UCA0RXBUF = message;
	__enable_interrupt();

	while(1){
//	    while((IFG2 && UCA0RXIFG) != 1);
//	    message = UCA0RXBUF;
//        while((IFG2 && UCA0TXIFG) != 1);
//        UCA0TXBUF = message;
////        P2OUT ^= LED;
//        __delay_cycles(700000);
	    writeUART('a');
        __delay_cycles(700000);
	}
	return 0;
}

void InitUART(rxd, txd){
    //step 1
    UCA0CTL1 |= UCSWRST;

    //step 2
    UCA0CTL0 = 0;
    UCA0CTL1 |= UCSSEL_2;
    //Baudrate registers
    //SMCLK = 1M , Baudrate = 38400
    //N = SMCLK / 38400 = 26.041
    //UCBRx = INT(N/16) = INT(1.62) = 2
    UCA0BR0 = 6;
    UCA0BR1 = 0;
    //modulation control register
    //UCBRFx = round((N/16 - INT(N/16)) * 16) = round(0.62*16) = 7
//    UCA0MCTL |= (UCBRF_7 | UCBRS_0 | UCOS16);
    UCA0MCTL = (UCBRF_8 | UCBRS_0 | UCOS16);

    //step 3
    P1SEL |= (rxd | txd);
    P1SEL2 |= (rxd | txd);

    UC0IE |= UCA0RXIE /*| UCA0TXIE*/;

    //step 4
    UCA0CTL1 &= ~UCSWRST;
}

void writeUART(char message){
    while(UCA0STAT & UCBUSY);
    UCA0TXBUF = message;
    while(UCA0STAT & UCBUSY);
}

